<?php

use Illuminate\Database\Seeder;

class ExpensesCategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Category::insert([
            'name' => 'Food',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Bills',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Transportation',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Entertainment',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Shopping',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Health',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Sport',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Education',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Pet',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Electronics',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Beauty',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
        App\Category::insert([
            'name' => 'Others',
            'description' => 'Category',
            'type' => 'Expenses'
        ]);
    }
}
